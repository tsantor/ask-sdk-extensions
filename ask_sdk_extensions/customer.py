# -*- coding: utf-8 -*-

import logging
import re
from collections import namedtuple

import requests

from . import exceptions

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class CustomerInfo:
    """
    Get customer contact information
    https://developer.amazon.com/docs/custom-skills/device-address-api.html
    https://developer.amazon.com/docs/custom-skills/request-customer-contact-information-for-use-in-your-skill.html

    EXAMPLE USAGE:

    # Check if we have access to customer info for a better user experience
    ci = CustomerInfo(handler_input)
    requested_perms = ci.get_missing_perms(
        ["given_name", "email", "mobile_number", "address"]
    )
    if requested_perms:
        perms = human_join([p.label for p in requested_perms])
        speech = f"You have refused to allow {SKILL_NAME} access to your \
                {perms}. {SKILL_NAME} will have limited functionality without \
                this information. Open the Alexa app and navigate to the \
                activity page to grant me permission.."
        card = {
            "type": "AskForPermissionsConsent",
            "permissions": [p.perm for p in requested_perms]
        }
        handler_input.response_builder.speak(speech).set_card(card)
        return handler_input.response_builder.response
    """

    _full_address = None
    _country_postal = None
    _full_name = None
    _given_name = None
    _email = None
    _mobile_number = None

    def __init__(self, handler_input):
        self.system = handler_input.request_envelope.context.system
        self.api_endpoint = self.system.api_endpoint

    @property
    def full_address(self):
        if self._full_address:
            return self._full_address
        device_id = self.system.device.device_id
        url = f"{self.api_endpoint}/v1/devices/{device_id}/settings/address"
        self._full_address = self.http_get(url)
        # logger.info(self._full_address)
        return self._full_address

    @property
    def country_postal(self):
        if self._country_postal:
            return self._country_postal
        device_id = self.system.device.device_id
        url = f"{self.api_endpoint}/v1/devices/{device_id}/settings/address/countryAndPostalCode"
        self._country_postal = self.http_get(url)
        # logger.info(self._country_postal)
        return self._country_postal

    @property
    def full_name(self):
        if self._full_name:
            return self._full_name
        url = f"{self.api_endpoint}/v2/accounts/~current/settings/Profile.name"
        self._full_name = self.http_get(url)
        # logger.info(self._full_name)
        return self._full_name

    @property
    def given_name(self):
        if self._given_name:
            return self._given_name
        url = f"{self.api_endpoint}/v2/accounts/~current/settings/Profile.givenName"
        self._given_name = self.http_get(url)
        # logger.info(self._given_name)
        return self._given_name

    @property
    def email(self):
        if self._email:
            return self._email
        url = f"{self.api_endpoint}/v2/accounts/~current/settings/Profile.email"
        self._email = self.http_get(url)
        # logger.info(self._email)
        return self._email

    @property
    def mobile_number(self):
        if self._mobile_number:
            return self._mobile_number
        url = f"{self.api_endpoint}/v2/accounts/~current/settings/Profile.mobileNumber"
        self._mobile_number = self.http_get(url)
        # logger.info(self._mobile_number)
        return self._mobile_number

    def postal_code(self, short=False):
        """Get postal code from full address or country/postal."""
        postal_code = None
        if self.has_address_access:
            postal_code = self.full_address.get("postalCode")
        elif self.has_country_postal_access:
            postal_code = self.country_postal.get("postalCode")
        if postal_code:
            if short:
                matches = re.match(r"(\d{5})", postal_code)
                if matches:
                    return matches.group(1)
            return postal_code

    def http_get(self, url):
        """Execute the authorized API request."""
        headers = {"Authorization": f"Bearer {self.system.api_access_token}"}
        r = requests.get(url, headers=headers)
        if r.status_code == 200:
            return r.json()
        elif r.status_code == 204:
            msg = "Query did not return any results."
            raise exceptions.NoContentError(msg)
        elif r.status_code == 401:
            msg = "Authentication token is malformed or invalid."
            raise exceptions.InvalidTokenError(msg)
        elif r.status_code == 403:
            msg = "Authentication token does not have access to the resource."
            raise exceptions.UnauthorizedError(msg)
        elif r.status_code == 429:
            msg = "Throttled due to an excessive number of requests."
            raise exceptions.TooManyRequestsError(msg)
        elif r.status_code == 500:
            msg = "Unexpected error occurred."
            raise exceptions.InternalServerError(msg)

    @property
    def has_address_access(self):
        """alexa:device:all:address:read"""
        try:
            self.full_address
            return True
        except exceptions.UnauthorizedError:
            return False

    @property
    def has_country_postal_access(self):
        """alexa:devices:all:address:country_and_postal_code:read"""
        try:
            self.country_postal
            return True
        except exceptions.UnauthorizedError:
            return False

    @property
    def has_full_name_access(self):
        """alexa::profile:name:read"""
        try:
            self.full_name
            return True
        except exceptions.UnauthorizedError:
            return False

    @property
    def has_given_name_access(self):
        """alexa::profile:given_name:read"""
        try:
            self.given_name
            return True
        except exceptions.UnauthorizedError:
            return False

    @property
    def has_email_access(self):
        """alexa::profile:email:read"""
        try:
            self.email
            return True
        except exceptions.UnauthorizedError:
            return False

    @property
    def has_mobile_number_access(self):
        """alexa::profile:mobile_number:read"""
        try:
            self.mobile_number
            return True
        except exceptions.UnauthorizedError:
            return False

    def get_perm_tuple(self, key):
        """Return named tuple of (label, perm)."""
        Perm = namedtuple("Perm", "label perm")
        if key == "address":
            # Note: in skill.json it accepts "alexa::devices:all:address:full:read"
            # here it wants the below value or we get an error
            return Perm("address", "read::alexa:device:all:address")
        if key == "country_and_postal_code":
            # Note: in skill.json it accepts "alexa::devices:all:address:country_and_postal_code:read"
            # here it wants the below value or we get an error
            return Perm(
                "postal code", "read::alexa:device:all:address:country_and_postal_code"
            )
        if key == "name":
            return Perm("full name", "alexa::profile:name:read")
        if key == "given_name":
            return Perm("first name", "alexa::profile:given_name:read")
        if key == "email":
            return Perm("email", "alexa::profile:email:read")
        if key == "mobile_number":
            return Perm("mobile number", "alexa::profile:mobile_number:read")

    def get_missing_perms(self, key_list):
        """Return a human readable list of missing pems for Alexa to speak."""
        requested_perms = []
        # Order is important as it makes most logical sense when speaking.
        if "address" in key_list:
            if not self.has_address_access:
                requested_perms.append(self.get_perm_tuple("address"))
        if "country_and_postal_code" in key_list:
            if not self.has_country_postal_access:
                requested_perms.append(self.get_perm_tuple("country_and_postal_code"))
        if "name" in key_list:
            if not self.has_full_name_access:
                requested_perms.append(self.get_perm_tuple("name"))
        if "given_name" in key_list:
            if not self.has_given_name_access:
                requested_perms.append(self.get_perm_tuple("given_name"))
        if "email" in key_list:
            if not self.has_email_access:
                requested_perms.append(self.get_perm_tuple("email"))
        if "mobile_number" in key_list:
            if not self.has_mobile_number_access:
                requested_perms.append(self.get_perm_tuple("mobile_number"))
        logger.info([p.perm for p in requested_perms])
        return requested_perms

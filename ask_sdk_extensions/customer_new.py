# -*- coding: utf-8 -*-

import logging
import re
from collections import namedtuple

import requests

from ask_sdk_model.services import ServiceException

# from ask_sdk_model.services.ups.error import Error
from . import exceptions

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


# DO NOT USE: For some reason it throws a "ACCESS_DENIED' is not a valid ErrorCode"
class CustomerInfo:
    """
    Get customer contact information. Wrapper around DeviceAddressServiceClient
    and UpsServiceClient.
    """

    _full_address = None
    _country_postal = None
    _full_name = None
    _given_name = None
    _email = None
    _mobile_number = None

    UNAUTHORIZED = "Authentication token does not have access to the resource."

    def __init__(self, handler_input):
        req_envelope = handler_input.request_envelope
        self.device_id = req_envelope.context.system.device.device_id
        self.service_client_fact = handler_input.service_client_factory
        self.dac = self.service_client_fact.get_device_address_service()
        self.upc = self.service_client_fact.get_ups_service()

    @property
    def full_address(self):
        if self._full_address:
            return self._full_address
        self._full_address = self.dac.get_full_address(self.device_id)
        # logger.info(self._full_address)
        return self._full_address

    @property
    def country_postal(self):
        if self._country_postal:
            return self._country_postal
        self._country_postal = self.dac.get_country_and_postal_code(self.device_id)
        # logger.info(self._country_postal)
        return self._country_postal

    @property
    def full_name(self):
        if self._full_name:
            return self._full_name
        self._full_name = self.upc.get_profile_name()
        # logger.info(self._full_name)
        return self._full_name

    @property
    def given_name(self):
        if self._given_name:
            return self._given_name
        self._given_name = self.upc.get_profile_given_name()
        # logger.info(self._given_name)
        return self._given_name

    @property
    def email(self):
        if self._email:
            return self._email
        self._email = self.upc.get_profile_email()
        # logger.info(self._email)
        return self._email

    @property
    def mobile_number(self):
        if self._mobile_number:
            return self._mobile_number
        self._mobile_number = self.upc.get_profile_mobile_number()
        # logger.info(self._mobile_number)
        return self._mobile_number

    def postal_code(self, short=False):
        """Get postal code from full address or country/postal."""
        postal_code = None
        if self.has_address_access:
            postal_code = self.full_address.get("postalCode")
        elif self.has_country_postal_access:
            postal_code = self.country_postal.get("postalCode")
        if postal_code:
            if short:
                matches = re.match(r"(\d{5})", postal_code)
                if matches:
                    return matches.group(1)
            return postal_code

    @property
    def has_address_access(self):
        """alexa:device:all:address:read"""
        try:
            self.full_address
            return True
        except ServiceException:
            return False

    @property
    def has_country_postal_access(self):
        """alexa:devices:all:address:country_and_postal_code:read"""
        try:
            self.country_postal
            return True
        except ServiceException:
            return False

    @property
    def has_full_name_access(self):
        """alexa::profile:name:read"""
        try:
            self.full_name
            return True
        except ServiceException:
            return False

    @property
    def has_given_name_access(self):
        """alexa::profile:given_name:read"""
        try:
            self.given_name
            return True
        except ServiceException:
            return False

    @property
    def has_email_access(self):
        """alexa::profile:email:read"""
        try:
            self.email
            return True
        except ServiceException:
            return False

    @property
    def has_mobile_number_access(self):
        """alexa::profile:mobile_number:read"""
        try:
            self.mobile_number
            return True
        except ServiceException:
            return False

    def get_perm_tuple(self, key):
        """Return named tuple of (label, perm)."""
        Perm = namedtuple("Perm", "label perm")
        if key == "address":
            # Note: in skill.json it accepts "alexa::devices:all:address:full:read"
            # here it wants the below value or we get an error
            return Perm("address", "read::alexa:device:all:address")
        if key == "country_and_postal_code":
            # Note: in skill.json it accepts "alexa::devices:all:address:country_and_postal_code:read"
            # here it wants the below value or we get an error
            return Perm(
                "postal code", "read::alexa:device:all:address:country_and_postal_code"
            )
        if key == "name":
            return Perm("full name", "alexa::profile:name:read")
        if key == "given_name":
            return Perm("first name", "alexa::profile:given_name:read")
        if key == "email":
            return Perm("email", "alexa::profile:email:read")
        if key == "mobile_number":
            return Perm("mobile number", "alexa::profile:mobile_number:read")

    def get_missing_perms(self, key_list):
        """Return a human readable list of missing pems for Alexa to speak."""
        requested_perms = []
        # Order is important as it makes most logical sense when speaking.
        if "address" in key_list:
            if not self.has_address_access:
                requested_perms.append(self.get_perm_tuple("address"))
        if "country_and_postal_code" in key_list:
            if not self.has_country_postal_access:
                requested_perms.append(self.get_perm_tuple("country_and_postal_code"))
        if "name" in key_list:
            if not self.has_full_name_access:
                requested_perms.append(self.get_perm_tuple("name"))
        if "given_name" in key_list:
            if not self.has_given_name_access:
                requested_perms.append(self.get_perm_tuple("given_name"))
        if "email" in key_list:
            if not self.has_email_access:
                requested_perms.append(self.get_perm_tuple("email"))
        if "mobile_number" in key_list:
            if not self.has_mobile_number_access:
                requested_perms.append(self.get_perm_tuple("mobile_number"))
        return requested_perms

# -*- coding: utf-8 -*-


class NoContentError(Exception):
    pass


class InvalidTokenError(Exception):
    pass


class UnauthorizedError(Exception):
    pass


class TooManyRequestsError(Exception):
    pass


class InternalServerError(Exception):
    pass

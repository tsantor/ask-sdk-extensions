# -*- coding: utf-8 -*-

import logging
from datetime import datetime, time

import requests
from dateutil import tz

from ask_sdk_core.utils import get_api_access_token

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def speak_time_of_day(tz_string="US/Eastern"):
    """Return the proper word for the time of day."""
    now = datetime.now().astimezone(tz.gettz(tz_string))
    now_time = now.time()

    # Morning is from 6 AM to 11:59 AM
    if now_time >= time(6, 00, 00) and now_time <= time(11, 59, 59):
        tod = "morning"
    # Afternoon is from 12:00 PM to around 4:59 PM.
    elif now_time >= time(12, 00, 00) and now_time <= time(16, 59, 59):
        tod = "afternoon"
    # Evening is from 5:00 PM to 7:59 PM, or around sunset.
    elif now_time >= time(17, 00, 00) and now_time <= time(19, 59, 59):
        tod = "evening"
    # Night is from sunset to sunrise, so from 8:00 PM until 5:59 AM.
    else:
        tod = "night"

    # logger.info(f"Time of day: {now_time} {tz_string} => {tod}")
    return tod


speak_tod = speak_time_of_day


def speak_date(_datetime):
    """Returns a date in a clear, consistent, speakable manner."""
    return _datetime.strftime("%A, %B %d, %Y at %-I:%M %p")


def speak_list(listed):
    """Return foo, bar and baz, rather than foo, bar, baz."""
    if len(listed) == 0:
        return ""
    if len(listed) == 1:
        return listed[0]
    if len(listed) == 2:
        return listed[0] + " and " + listed[1]
    return ", ".join(listed[:-1]) + ", and " + listed[-1]


def speak_progressive_response(
    handler_input, speech="Ok, give me a minute while I process your request. "
):
    api_end_point = handler_input.request_envelope.context.system.api_endpoint
    access_token = get_api_access_token(handler_input)
    request_id = handler_input.request_envelope.request.request_id
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer {}".format(access_token),
    }
    body = {
        "header": {"requestId": "{}".format(request_id)},
        "directive": {"type": "VoicePlayer.Speak", "speech": speech},
    }
    progressive_url = f"{api_end_point}/v1/directives"
    r = requests.post(progressive_url, json=body, headers=headers)
    r.raise_for_status()

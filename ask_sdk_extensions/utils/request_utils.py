# -*- coding: utf-8 -*-

import logging

from ask_sdk_model.intent_request import IntentRequest
from ask_sdk_core.utils import get_slot

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# -----------------------------------------------------------------------------
# Session
# -----------------------------------------------------------------------------


def get_session_value(handler_input, key_name):
    """
    Return the session attribute.

    :param handler_input: The handler input instance that is generally
        passed in the sdk's request and exception components
    :type handler_input: ask_sdk_core.handler_input.HandlerInput
    :param key_name: Name of the key for which the value has to be retrieved
    :type key_name: str
    :return: Key value for the provided key if it exists
    """
    attributes = handler_input.attributes_manager.session_attributes
    if attributes and key_name in attributes:
        return attributes.get(key_name, None)
    return None


# -----------------------------------------------------------------------------
# Persistent
# -----------------------------------------------------------------------------


def get_persistent_value(handler_input, key_name):
    """
    Return the persistent attribute.

    :param handler_input: The handler input instance that is generally
        passed in the sdk's request and exception components
    :type handler_input: ask_sdk_core.handler_input.HandlerInput
    :param key_name: Name of the key for which the value has to be retrieved
    :type key_name: str
    :return: Key value for the provided key if it exists
    """
    attributes = handler_input.attributes_manager.persistent_attributes
    if attributes and key_name in attributes:
        return attributes.get(key_name, None)
    return None


# -----------------------------------------------------------------------------
# Request
# -----------------------------------------------------------------------------


def get_intent(handler_input):
    """Return intent."""
    request = handler_input.request_envelope.request
    if isinstance(request, IntentRequest):
        return request.intent

    raise TypeError("The provided request is not an IntentRequest")


def is_intent_request(handler_input):
    """Return True if this is a intent request."""
    request = handler_input.request_envelope.request
    if isinstance(request, IntentRequest):
        return True
    return False


def get_slots(handler_input):
    """Return intent slots."""
    request = handler_input.request_envelope.request
    if isinstance(request, IntentRequest):
        if request.intent.slots is not None:
            return request.intent.slots

    raise TypeError("The provided request is not an IntentRequest")


# -----------------------------------------------------------------------------
# Slots
# -----------------------------------------------------------------------------


def get_slot_resolution_id(handler_input, slot_name):
    slot = get_slot(handler_input=handler_input, slot_name=slot_name)
    if slot is not None and slot.resolutions is not None:
        return slot.resolutions.resolutions_per_authority[0].values[0].value.id

    raise ValueError("Provided slot {} doesn't have resolutions".format(slot_name))


def get_slot_resolution_name(handler_input, slot_name):
    slot = get_slot(handler_input=handler_input, slot_name=slot_name)
    if slot is not None and slot.resolutions is not None:
        return slot.resolutions.resolutions_per_authority[0].values[0].value.name

    raise ValueError("Provided slot {} doesn't have resolutions".format(slot_name))

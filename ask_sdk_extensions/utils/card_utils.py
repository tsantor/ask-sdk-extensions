# -*- coding: utf-8 -*-

import logging
import re

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def strip_ssml(text):
    regex = r"<[^>]+>"
    return re.sub(regex, "", text, 0, re.MULTILINE)

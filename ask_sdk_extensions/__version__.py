__pip_package_name__ = "ask-sdk-extensions"
__description__ = (
    "The ASK SDK extensions provides extra Alexa Skills Kit "
    "functionality, helpdule for building Alexa Skills."
)
__url__ = "https://bitbucket.org/tsantor/ask-sdk-extensions"
__version__ = "0.0.8"
__author__ = "Tim Santor"
__author_email__ = "tsantor@xstudios.agency"
__license__ = "MIT"
__keywords__ = ["ASK SDK", "Alexa Skills Kit", "Alexa", "ASK SDK Extentions"]
__install_requires__ = [
    "requests",
    "python_dateutil",
    "ask-sdk-model>=1.0.0",
    "ask-sdk-runtime>=1.1.0",
]
